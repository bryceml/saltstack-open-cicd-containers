FROM oraclelinux:8

RUN dnf install -y \
    vim-enhanced \
    gcc \
    rpm-build \
    rpm-devel \
    rpmlint \
    make \
    bash \
    diffutils \
    patch \
    rpmdevtools \
    git \
    redhat-rpm-config \
    rpmdevtools \
    xz \
    libffi-devel \
    bzip2-devel \
    xz-devel \
    ncurses-devel \
    gdbm-devel \
    sqlite-devel \
    readline-devel \
    zlib-devel \
    libuuid-devel \
    openssl-devel \
    xmlsec1-openssl \
    libtool-ltdl-devel \
    ruby-devel \
    rubygems \
    wget \
    && dnf groupinstall -y 'Development Tools' \
    && rm -rf /var/cache/dnf/*

# http://bugs.python.org/issue19846
# > At the moment, setting "LANG=C" on a Linux system *fundamentally breaks Python 3*, and that's not OK.
ENV LANG C.UTF-8
ENV PYVER '3.7.12'
ENV PYPRE '3.7'
ENV PYSHORT '37'

RUN curl -LO https://www.python.org/ftp/python/${PYVER}/Python-${PYVER}.tar.xz \
    && tar xvf Python-${PYVER}.tar.xz \
    && cd Python-${PYVER} \
    && ./configure --with-system-ffi --enable-shared \
    && make  \
    && make install  \
    && cd ..  \
    && rm -rf Python-${PYVER}.tar.xz Python-${PYVER} \
    && ldconfig /usr/local/lib

RUN gem install --no-document fpm
