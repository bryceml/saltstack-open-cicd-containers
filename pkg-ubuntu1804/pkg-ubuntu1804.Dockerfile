FROM ubuntu:18.04

ENV DEBIAN_FRONTEND noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN true

RUN apt-get update \
    && apt-get install -y \
    gpg \
    vim \
    gcc \
    make \
    bash \
    coreutils \
    diffutils \
    patch \
    git \
    libffi-dev \
    libbz2-dev \
    xz-utils \
    libncurses5-dev \
    libgdbm-dev \
    libsqlite3-dev \
    libreadline-dev \
    zlib1g-dev \
    uuid-dev \
    devscripts \
    pbuilder \
    ruby \
    ruby-dev \
    rubygems \
    wget \
    curl \
    libxmlsec1-openssl \
    libxmlsec1-dev \
    libltdl-dev \
    build-essential \
    bash-completion \
    dh-systemd \
    && rm -rf /var/lib/apt/lists/*

# http://bugs.python.org/issue19846
# > At the moment, setting "LANG=C" on a Linux system *fundamentally breaks Python 3*, and that's not OK.
ENV LANG C.UTF-8
ENV PYVER '3.7.12'
ENV PYPRE '3.7'
ENV PYSHORT '37'
ENV OPENSSLVER '1.1.1l'

RUN gem install --no-document fpm

RUN curl -LO https://www.openssl.org/source/openssl-${OPENSSLVER}.tar.gz \
    && tar -xvf openssl-${OPENSSLVER}.tar.gz \
    && cd openssl-${OPENSSLVER} \
    && ./config shared --prefix=/usr/local/openssl11 --openssldir=/usr/local/openssl11 \
    && make \
    && make install \
    && cd .. \
    && rm -rf openssl-${OPENSSLVER}.tar.gz openssl-${OPENSSLVER} \
    && echo /usr/local/openssl11/lib >> /etc/ld.so.conf.d/local-openssl11.conf \
    && ldconfig

RUN curl -LO https://www.python.org/ftp/python/${PYVER}/Python-${PYVER}.tar.xz \
    && tar xvf Python-${PYVER}.tar.xz \
    && cd Python-${PYVER} \
    && LDFLAGS="-Wl,-rpath=/usr/local/openssl11/lib" ./configure --with-openssl=/usr/local/openssl11 --with-system-ffi --enable-shared \
    && make  \
    && make install  \
    && cd ..  \
    && rm -rf Python-${PYVER}.tar.xz Python-${PYVER} \
    && ldconfig
